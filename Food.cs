﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace McOrders {
  public class Food {
    public Food(int preparingTime) {
      PreparingTime = preparingTime;
    }
    public int PreparingTime { get; set; }
  }
}
