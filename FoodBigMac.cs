﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace McOrders {
  public class FoodBigMac : Food {
    public FoodBigMac() : base(10) { }

    public override string ToString() {
      return "Big Mac";
    }
  }
}
