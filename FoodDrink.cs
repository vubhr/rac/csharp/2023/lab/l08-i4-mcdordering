﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace McOrders {
  public class FoodDrink : Food {
    public FoodDrink() : base(2) { }

    public override string ToString() {
      return "Drink";
    }
  }
}
