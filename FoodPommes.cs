﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace McOrders {
  public class FoodPommes : Food {
    public FoodPommes() : base(5) { }

    public override string ToString() {
      return "Pommes";
    }
  }
}
