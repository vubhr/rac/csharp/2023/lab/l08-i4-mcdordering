﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace McOrders {
  public class Order {
    public Order(int id, Food food) {
      Id = id;
      Food = food;
    }
    public int Id { get; set; }
    public Food Food { get; set; }

    public override string ToString() {
      return $"{string.Format("{0:D3}", Id)} ({Food.ToString()})";
    }
  }
}
