﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace McOrders {
  public class OrderArgs : EventArgs {
    public OrderArgs(Order order) {
      Order = order;
    }
    public Order Order { get; set; }
  }
}
