﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace McOrders {
  public class Ordering {
    public Ordering() {
      nextOrderId = 1;
      ActiveOrders = new List<Order>();
      CompletedOrders = new List<Order>();
    }

    public void AddOrder(Food food) {
      var order = new Order(nextOrderId++, food);
      Task task = Task.Run(() => {
        Thread.Sleep(2000);
        StartOrder(order);
        Thread.Sleep(order.Food.PreparingTime * 1000);
        CompleteOrder(order);
      });
    }

    public void StartOrder(Order order) {
      ActiveOrders.Add(order);
      OrderStarted?.Invoke(this, new OrderArgs(order));
    }

    public void CompleteOrder(Order order) {
      ActiveOrders.Remove(order);
      CompletedOrders.Add(order);
      OrderCompleted?.Invoke(this, new OrderArgs(order));
    }

    public delegate void OrderingDelegate(object sender, OrderArgs e);
    public event OrderingDelegate OrderStarted;
    public event OrderingDelegate OrderCompleted;

    public List<Order> ActiveOrders { get; }
    public List<Order> CompletedOrders { get; }

    private int nextOrderId;
  }
}
